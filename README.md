# Breadth of coverage calculation workflow
Description of method of Langner et al, 2021. PLOS Genetics. Genomic rearrangements generate hypervariable mini-chromosomes in host-specific isolates of the blast fungus.
https://doi.org/10.1371/journal.pgen.1009386

# 1. Create reference files for core contigs / minichromosome contigs
```bash
bwa index contig.fasta
```
# 2. Mapping reads and mark duplicates
## 2.1a. Mapping single-end reads
\* The variable $reference corresponds to the indexed contig file (step 1)
```bash
bwa mem $reference $reads -R @RG\\tID:$sample\\tSM\:$sample -t $threads > $sample.to"_"$refname.sam
samtools view -@ $threads -ShbF 4 $sample.to"_"$refname.sam | samtools sort -@ $threads - > $sample.mapped_to"_"$refname.bam
rm $sample.to"_"$refname.sam
```
## 2.1b. Mapping pair-end reads
```bash
bwa mem $reference $read1 $read2 -R @RG\\tID:$sample\\tSM\:$sample -t $threads > $sample.to"_"$refname.sam
samtools view -@ $threads -ShbF 4 $sample.to"_"$refname.sam | samtools sort -@ $threads - > $sample.mapped_to"_"$refname.bam
rm $sample.to"_"$refname.sam
```
## 2.2. Mark duplicates
```bash
java -jar picard.jar MarkDuplicates I=$sample.mapped_to"_"$refname.bam O=$sample.mapped_to"_"$refname.dd.bam M=$sample.$refname.metrics.txt AS=true
rm $sample.mapped_to"_"$refname.bam
```
## 2.3. Index bam file
```bash
samtools index $sample.mapped_to"_"$refname.dd.bam
```

# 3. Calculating breadths of coverage
Given an indexed $bam file (step 2), exatrct names and lengths of contigs in a temporary file
```bash
samtools view -H $bam | grep ^@SQ | cut -f2- | sed 's/SN://g' | sed 's/LN://g' > $bam.contigs.tmp
```
Loop over each contig and calculate the breadth of coverage for at least 1X
Set the variable $mapQ to the desired Mapping Quality filter
```bash
mapQ=0
while read line; do
    contig=$(echo $line | cut -f1 -d " ")
    length=$(echo $line | cut -f2 -d " ")
    missing=$(samtools depth -Q $mapQ -a $bam -r $contig | awk '$3 == 0' | wc -l)
    cover=$(python -c "print(round(((1 - ($missing / float($length))) * 100),2))")
    echo -e "$contig\t$mapQ\t$cover"
done < $bam.contigs.tmp
```
Remove temporary file
```bash
rm $bam.contigs.tmp
```
